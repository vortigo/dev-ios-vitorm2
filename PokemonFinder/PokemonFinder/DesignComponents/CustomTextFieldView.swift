//
//  CustomTextFieldView.swift
//  PokemonFinder
//
//  Created by Vitor Demenighi on 14/11/20.
//

import UIKit

protocol CustomTextFieldViewDelegate {
    func showPokemonTypesActionSheet()
}

@IBDesignable
final class CustomTextFieldView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var titleLabel: DescriptionLabel!
    @IBOutlet weak var mainTextField: UITextField!
    @IBOutlet weak var arrowButton: UIButton!
    @IBOutlet weak var hiddenArrowButton: UIButton!
    
    var delegate: CustomTextFieldViewDelegate?
    
    var isPokemonTypeInput: Bool = false {
        didSet {
            if isPokemonTypeInput {
                arrowButton.isHidden = false
                hiddenArrowButton.isHidden = false
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadXib() 
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadXib()
    }

    func loadXib() {
        Bundle.main.loadNibNamed("CustomTextFieldView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        setup()
    }
    
    func setup(){
        mainTextField.tintColor = .white
        mainTextField.textColor = .white
        mainTextField.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        mainTextField.returnKeyType = .done
        mainTextField.delegate = self
    }
    
    @IBAction func arrowButtonTouched(_ sender: Any) {
        self.delegate?.showPokemonTypesActionSheet()
    }
}

extension CustomTextFieldView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        mainTextField.resignFirstResponder()
        return false
    }
}


