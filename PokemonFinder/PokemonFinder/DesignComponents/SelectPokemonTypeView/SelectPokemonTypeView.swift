//
//  SelectPokemonTypeView.swift
//  PokemonFinder
//
//  Created by Vitor Demenighi on 14/11/20.
//

import UIKit

protocol SelectPokemonTypeViewDelegate {
    func didSelectType(type: String)
    func showAlert(message: String)
}

@IBDesignable
final class SelectPokemonTypeView: UIView {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var pokemonTypes: [PokemonType] = []
    var selectedType: String = ""
    
    var delegate: SelectPokemonTypeViewDelegate?
    
    @IBOutlet var contentView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadXib()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadXib()
    }
    
    func loadXib() {
        Bundle.main.loadNibNamed("SelectPokemonTypeView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        setup()
    }
    
    func setup(){
        containerView.layer.cornerRadius = 5
        tableView.layer.cornerRadius = 5
        tableView.backgroundColor = .white
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(type: SelectPokemonTypeTableViewCell.self)
    }
    
    @IBAction func closeButtonTouched(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    @IBAction func confirmButtonTouched(_ sender: Any) {
        if selectedType != "" {
            delegate?.didSelectType(type: selectedType)
        } else {
            delegate?.showAlert(message: "Selecione um tipo de Pokémon")
        }
    }
    
}

extension SelectPokemonTypeView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemonTypes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SelectPokemonTypeTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        cell.isRadioButtonSelected = selectedType == pokemonTypes[indexPath.row].name
        cell.pokemonType = pokemonTypes[indexPath.row]
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
}

extension SelectPokemonTypeView: SelectPokemonTypeTableViewCellDelegate {
    
    func didSelectPokemonType(selectedType: String) {
        self.selectedType = selectedType
        tableView.reloadData()
    }
}

