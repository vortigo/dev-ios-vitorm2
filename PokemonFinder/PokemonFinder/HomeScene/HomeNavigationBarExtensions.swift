//
//  HomeNavigationBar.swift
//  PokemonFinder
//
//  Created by Vitor Demenighi on 15/11/20.
//

import UIKit

extension HomeViewController {
    
    func setupNavigationBar(){
        editBarButton = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(editBarButtonTouched))
        editBarButton.tintColor = .white
        searchBarButton = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(searchBarButtonTouched))
        searchBarButton.tintColor = .white
        cancelBarButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelBarButtonTouched))
        cancelBarButton.tintColor = .white
        
        navigationItem.leftBarButtonItem = editBarButton
        navigationItem.rightBarButtonItem = searchBarButton
    }
    
    @objc func searchBarButtonTouched(){
        navigationItem.leftBarButtonItem = nil
        navigationItem.rightBarButtonItem = nil
        
        searchBar.text = ""
        searchBar.searchTextField.backgroundColor = .white
        searchBar.searchTextField.textColor = .black
        searchBar.placeholder = "Search"
        searchBar.tintColor = #colorLiteral(red: 0.0862745098, green: 0.7803921569, blue: 0.631372549, alpha: 1)
        searchBar.scopeBarButtonTitleTextAttributes(for: .normal)
        searchBar.delegate = self
        
        navigationItem.titleView = searchBar
        navigationItem.rightBarButtonItem = cancelBarButton
    }
    
    @objc func editBarButtonTouched(){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let logoutAction = UIAlertAction(title: "Logout", style: .default, handler: { _ in
            self.viewModel.logout()
        })
        let selectFavoriteTypeAction = UIAlertAction(title: "Select Favorite Type", style: .default, handler: { _ in
            self.showSelectPokemonTypeView()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(logoutAction)
        alert.addAction(selectFavoriteTypeAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func cancelBarButtonTouched(){
        viewModel.searchPokemon(value: "")
        navigationItem.titleView = nil
        navigationItem.rightBarButtonItem = nil
        setupNavigationBar()
    }
    
    func showSelectPokemonTypeView(){
        guard let user = viewModel.user else { return }
        selectPokemonTypeView.delegate = self
        selectPokemonTypeView.constraintFully(to: view)
        selectPokemonTypeView.pokemonTypes = viewModel.pokemonTypes
        selectPokemonTypeView.selectedType = user.favoritePokemonType
        
        view.addSubview(selectPokemonTypeView)
        self.navigationController?.navigationBar.isUserInteractionEnabled = false
    }
}


extension HomeViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.searchPokemon(value: searchText)
    }
}

extension HomeViewController: SelectPokemonTypeViewDelegate {
    func didSelectType(type: String) {
        viewModel.updateUserInfo(type: type)
    }
    
    func showAlert(message: String) {
        present(message: message)
    }
}

