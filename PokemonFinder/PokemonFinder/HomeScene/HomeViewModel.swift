//
//  HomeViewModel.swift
//  PokemonFinder
//
//  Created by Vitor Demenighi on 15/11/20.
//

import Foundation

protocol HomeViewModelDelegate {
    func didGetUserInfo(user: User)
    func didGetPokemonTypes()
    func didUpdatePokemons()
    func didUpdateFavoriteType(type: String)
    func logout()
    func failed(message: String)
}

class HomeViewModel {
    
    let service: Service
    
    var delegate: HomeViewModelDelegate?
    
    var user: User?
    var pokemonTypes: [PokemonType] = []
    var pokemons: [Pokemon] = []
    var initialPokemons: [Pokemon] = []
    var filterType: FilterType?
    
    init() {
        self.service = Service()
    }
    
    func getUserInfo(){
        let currentUser = service.getUserInfo()
        if let user = currentUser {
            self.user = user
            delegate?.didGetUserInfo(user: user)
        } else {
            delegate?.failed(message: "Failed to access user info")
        }
    }
    
    func updateUserInfo(type: String) {
        guard let user = self.user else { return }
        user.favoritePokemonType = type
        if service.updateUserInfo(user: user) {
            self.user = user
            self.delegate?.didUpdateFavoriteType(type: user.favoritePokemonType)
        } else {
            self.delegate?.failed(message: "Failed to register favorite type")
        }
    }
    
    func fetchPokemonTypes() {
        service.fetchPokemonTypes { (types, error) in
            if let pokemonTypes = types {
                self.pokemonTypes = pokemonTypes
                self.delegate?.didGetPokemonTypes()
            } else {
                self.delegate?.failed(message: "Failed to access pokémon types")
            }
        }
    }
    
    func fetchPokemons(with type: String) {
        service.fetchPokemons { (pokemons, error) in
            if let pokemonsResult = pokemons {
                self.pokemons = pokemonsResult.filter { $0.type?.contains(type) ?? false }
                if let type = self.filterType {
                    self.filterPokemons(with: type)
                } else {
                    self.initialPokemons = self.pokemons
                    self.delegate?.didUpdatePokemons()
                }
            } else {
                self.delegate?.failed(message: "Failed to access pokémons")
            }
        }
    }
    
    func filterPokemons(with type: FilterType) {
        self.filterType = type
        if type == .asc {
            pokemons.sort { ($0.name ?? "") > ($1.name ?? "")}
        } else {
            pokemons.sort { ($0.name ?? "") < ($1.name ?? "")}
        }
        self.initialPokemons = pokemons
        self.delegate?.didUpdatePokemons()
    }
    
    func logout(){
        service.logout()
        self.delegate?.logout()
    }
    
    func searchPokemon(value: String) {
        var resultSearch: [Pokemon] = []
        
        if value == "" {
            pokemons = initialPokemons
            self.delegate?.didUpdatePokemons()
            return
        }
        
        for pokemon in initialPokemons {
            if (pokemon.name?.lowercased() ?? "").contains(value.lowercased()) {
                resultSearch.append(pokemon)
            }
        }
        
        pokemons = resultSearch
        self.delegate?.didUpdatePokemons()
    }
    
}
