//
//  PokemonTableViewCell.swift
//  PokemonFinder
//
//  Created by Vitor Demenighi on 15/11/20.
//

import UIKit
import SDWebImage

class PokemonTableViewCell: UITableViewCell {

    @IBOutlet weak var pokemonImageBackgroundView: UIView!
    @IBOutlet weak var pokemonImage: UIImageView!
    @IBOutlet weak var pokemonNameLabel: UILabel!
    
    var pokemon: Pokemon? {
        didSet{
            guard let pokemon = self.pokemon else { return }
            if let pokemonImageURL = pokemon.thumbnailImage {
                pokemonImage.sd_setImage(with: URL(string: pokemonImageURL), placeholderImage: nil)
            }
            pokemonImageBackgroundView.clipsToBounds = true
            pokemonImageBackgroundView.layer.cornerRadius = pokemonImageBackgroundView.frame.width/2
            pokemonNameLabel.text = pokemon.name
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .white
    }
    
}
