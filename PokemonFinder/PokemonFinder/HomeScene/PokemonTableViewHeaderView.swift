//
//  PokemonTableViewHeaderView.swift
//  PokemonFinder
//
//  Created by Vitor Demenighi on 15/11/20.
//

import UIKit

enum FilterType {
    case des
    case asc
}

protocol PokemonTableViewHeaderViewDelegate {
    func nameFilterClicked(type: FilterType)
}

class PokemonTableViewHeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var arrowImage: UIImageView!
    var delegate: PokemonTableViewHeaderViewDelegate?
    
    var type: FilterType = .asc {
        didSet {
            self.arrowImage.image = UIImage(named: type == .asc ? "arrowUp" : "arrowDown")
        }
    }
    
    static let reuseIdentifier: String = String(describing: self)
    
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: nil)
    }
    
    @IBAction func nameFilterButtonTouched(_ sender: Any) {
        self.delegate?.nameFilterClicked(type: self.type)
        self.type = self.type == .des  ? .asc : .des
    }
    
}
