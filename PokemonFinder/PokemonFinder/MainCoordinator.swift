//
//  MainCoordinator.swift
//  PokemonFinder
//
//  Created by Vitor Demenighi on 14/11/20.
//

import UIKit

class MainCoordinator {
    
    let navigationController: UINavigationController
    let service: Service
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.service = Service()
    }
    
    func start(){
        if service.isLogged() {
            navigationController.viewControllers = [HomeViewController(coordinator: self)]
        } else {
            navigationController.viewControllers = [InitialViewController(coordinator: self)]
        }
    }
    
    func goToSignupScreen(signupStage: SignupStageType, viewModel: SignupViewModel?){
        
        let signupViewController = SignupViewController(coordinator: self, signupStage: signupStage, viewModel:viewModel ?? SignupViewModel())
        
        if signupStage == .registerName {
            navigationController.setViewControllers([signupViewController], animated: true)
        } else {
            navigationController.pushViewController(signupViewController, animated: true)
        }
    }
    
    func goToHomeScreen(){
        navigationController.setViewControllers([HomeViewController(coordinator: self)], animated: true)
    }
    
    func logout(){
        navigationController.setViewControllers([InitialViewController(coordinator: self)], animated: true)
    }
    
}
