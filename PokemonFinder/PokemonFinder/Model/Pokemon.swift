//
//  Pokemon.swift
//  PokemonFinder
//
//  Created by Vitor Demenighi on 14/11/20.
//

import Foundation

class Pokemon: Codable {
    
    let abilities : [String]?
    let detailPageURL : String?
    let weight : Double?
    let weakness : [String]?
    let number : String?
    let height : Double?
    let collectibles_slug : String?
    let featured : String?
    let slug : String?
    let name : String?
    let thumbnailAltText : String?
    let thumbnailImage : String?
    let id : Int?
    let type : [String]?
}
