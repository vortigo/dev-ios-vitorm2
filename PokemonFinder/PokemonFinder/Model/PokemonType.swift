//
//  Type.swift
//  PokemonFinder
//
//  Created by Vitor Demenighi on 14/11/20.
//

import Foundation

class PokemonTypeCollection: Codable {
    let results: [PokemonType]
}

class PokemonType: Codable {
    
    let thumbnailImage: String
    let name: String
}


