//
//  Service.swift
//  PokemonFinder
//
//  Created by Vitor Demenighi on 15/11/20.
//

import Foundation

class Service {
    
    final let pokemonTypesURL = "https://vortigo.blob.core.windows.net/files/pokemon/data/types.json"
    final let pokemonsURL = "https://vortigo.blob.core.windows.net/files/pokemon/data/pokemons.json"
    final let userUserDefaultsURL = "userInfo"
    final let isLoggedURL = "isLogged"
    
    func registerUser(user: User) -> Bool {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(user) {
            UserDefaults.standard.set(encoded, forKey: userUserDefaultsURL)
            UserDefaults.standard.set(true, forKey: isLoggedURL)
            return true
        } else {
           return false
        }
    }
    
    func getUserInfo() -> User? {
        if let savedUser = UserDefaults.standard.object(forKey: userUserDefaultsURL) as? Data {
            let decoder = JSONDecoder()
            if let user = try? decoder.decode(User.self, from: savedUser) {
                return user
            }
        }
        
        return nil
    }
    
    func updateUserInfo(user: User) -> Bool {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(user) {
            UserDefaults.standard.set(encoded, forKey: userUserDefaultsURL)
            return true
        } else {
            return false
        }
    }
    
    func logout() {
        UserDefaults.standard.removeObject(forKey: isLoggedURL)
        UserDefaults.standard.removeObject(forKey: userUserDefaultsURL)
    }
    
    func isLogged() -> Bool {
        return UserDefaults.standard.bool(forKey: isLoggedURL)
    }
    
    func fetchPokemonTypes(completion: @escaping ([PokemonType]?, Error?) -> Void) {
        if let url = URL(string: pokemonTypesURL){
            let task = URLSession.shared.dataTask(with: url) { (data, request, err) in
                if err != nil {
                    completion(nil, err)
                } else if let returnData = data {
                    if let result = try? JSONDecoder().decode(PokemonTypeCollection.self, from: returnData) {
                        completion(result.results, nil)
                    } else {
                        completion(nil, err)
                    }
                }
            }
            task.resume()
        }
    }
    
    func fetchPokemons(completion: @escaping ([Pokemon]?, Error?) -> Void) {
        if let url = URL(string: pokemonsURL){
            let task = URLSession.shared.dataTask(with: url) { (data, request, err) in
                if err != nil {
                    completion(nil, err)
                } else if let returnData = data {
                    if let result: [Pokemon] = try? JSONDecoder().decode([Pokemon].self, from: returnData) {
                        let uniqueResult = result.uniques(by: \.id)
                        completion(uniqueResult, nil)
                    } else {
                        completion(nil, err)
                    }
                }
            }
            task.resume()
        }
    }
    
}
