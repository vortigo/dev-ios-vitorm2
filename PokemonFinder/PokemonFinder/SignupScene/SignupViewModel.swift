//
//  SignupViewModel.swift
//  PokemonFinder
//
//  Created by Vitor Demenighi on 14/11/20.
//

import Foundation

protocol SignupViewModelDelegate {
    func didGetPokemonTypes(types: [PokemonType])
    func didRegisterUser()
    func failed(message: String)
}

class SignupViewModel {
    
    var selectedPokemonType: String?
    var userName: String?
    
    var delegate: SignupViewModelDelegate?
    
    let service: Service
    
    init() {
        self.service = Service()
    }
    
    func fetchPokemonTypes() {
        service.fetchPokemonTypes { [weak self] (pokemonTypes, error) in
            if let resultPokemonTypes = pokemonTypes {
                self?.delegate?.didGetPokemonTypes(types: resultPokemonTypes)
            } else {
                if let errorResult = error {
                    self?.delegate?.failed(message: errorResult.localizedDescription)
                }
            }
        }
    }
    
    func registerUser(){
        if let favoritePokemonType = selectedPokemonType, let userName = userName {
            let user = User(userName: userName, favoritePokemonType: favoritePokemonType)
            if service.registerUser(user: user) {
                delegate?.didRegisterUser()
                return
            }
        }
         
        delegate?.failed(message: "Erro ao registrar treinador!")
    }
}
